﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace TheSwearJar.Models
{
    public class Jar
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(125)]
        [Required]
        public string Name { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public string UserId { get; set; }
        
        public virtual List<SwearInstance> SwearInstances { get; set; }

        public DateTime RecentlyActive { get; set; }

        [DataType(DataType.Currency)]
        public decimal TotalInJar
        {
            get
            {
                return SwearInstances.Sum(si => si.Cost);
            }
        }
    }
}
