﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSwearJar.Models
{
    public class SwearInstance
    {
        public SwearInstance()
        {
            CreatedDate = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Swear Swear { get; set; }

        public DateTime CreatedDate { get; set; }

        public decimal Cost { get; set; }
    }
}
