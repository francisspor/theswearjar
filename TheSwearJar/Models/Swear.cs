﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheSwearJar.Models
{
    public class Swear
    {
        public Swear()
        {
            CreatedDate = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name = "Swear")]
        [Required]
        public string Value { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        public string UserId { get; set; }

        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [Column(TypeName = "DateTime")]
        public DateTime CreatedDate { get; set; }

        [DataType(DataType.Currency)]
        [DefaultValue(0)]
        public decimal Cost { get; set; }
    }
}
