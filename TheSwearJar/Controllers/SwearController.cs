﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using TheSwearJar.Models;

namespace TheSwearJar.Controllers
{
    public partial class SwearController : BaseController
    {
        // GET: /Swear/
        public virtual ActionResult Index()
        {
            var id = User.Identity.GetUserId();

            var swearValues =
                DbContext.Swears.Where(sv => sv.UserId == id);

            return View(swearValues.OrderBy(sv=>sv.Value).ToList());
        }

        // GET: /Swear/Details/5
        public virtual ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Swear swear = DbContext.Swears.Find(id);
            if (swear == null)
            {
                return HttpNotFound();
            }
            return View(swear);
        }

        // GET: /Swear/Create
        public virtual ActionResult Create()
        {
            return View();
        }

        // POST: /Swear/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "Value,Cost")] Swear swear)
        {
            if (ModelState.IsValid)
            {
                swear.UserId = User.Identity.GetUserId();
                DbContext.Swears.Add(swear);
                DbContext.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(swear);
        }

        // GET: /Swear/Edit/5
        public virtual ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Swear swear = DbContext.Swears.Find(id);
            if (swear == null)
            {
                return HttpNotFound();
            }
            return View(swear);
        }

        // POST: /Swear/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "Id,Value,UserId,Cost")] Swear swear)
        {
            if (ModelState.IsValid)
            {
                DbContext.Entry(swear).State = EntityState.Modified;
                DbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(swear);
        }

        [HttpGet]
        public virtual ActionResult Delete(int id)
        {
            Swear swear = DbContext.Swears.Find(id);
            DbContext.Swears.Remove(swear);
            DbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
