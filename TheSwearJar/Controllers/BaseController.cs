﻿using System.Web.Mvc;
using TheSwearJar.Contexts;

namespace TheSwearJar.Controllers
{
    public abstract partial class BaseController : Controller
    {
        protected ApplicationDbContext DbContext = new ApplicationDbContext();
        protected BaseController()
        {
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DbContext.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}