﻿using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace TheSwearJar.Controllers
{
    public partial class HomeController : BaseController
    {
        public virtual ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction(MVC.Home.Dashboard());
            }
            return View();
        }

        [Authorize]
        public virtual ActionResult Dashboard()
        {
            var id = User.Identity.GetUserId();

            var jars = DbContext.Jars
                .Where(j => j.UserId == id)
                .OrderByDescending(j => j.RecentlyActive)
                .Take(4);
            return View(jars.ToList());
        }

        public virtual ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public virtual ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
    }
}