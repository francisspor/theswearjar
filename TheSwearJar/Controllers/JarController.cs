﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using TheSwearJar.Models;

namespace TheSwearJar.Controllers
{
    [Authorize]
    public partial class JarController : BaseController
    {

        // GET: /Jar/
        public virtual ActionResult Index()
        {
            var id = User.Identity.GetUserId();
            var jars = DbContext.Jars.Where(j=>j.UserId == id);
            return View(jars.ToList());
        }

        // GET: /Jar/Details/5
        public virtual ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jar jar = DbContext.Jars.Find(id);
            if (jar == null)
            {
                return HttpNotFound();
            }
            return View(jar);
        }

        // GET: /Jar/Create
        public virtual ActionResult Create()
        {
            return View();
        }

        // POST: /Jar/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create([Bind(Include = "Id,Name")] Jar jar)
        {
            if (ModelState.IsValid)
            {
                jar.UserId = User.Identity.GetUserId();
                DbContext.Jars.Add(jar);
                DbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(jar);
        }

        // GET: /Jar/Edit/5
        public virtual ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Jar jar = DbContext.Jars.Find(id);
            if (jar == null)
            {
                return HttpNotFound();
            }
            return View(jar);
        }

        // POST: /Jar/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Edit([Bind(Include = "Id,Name,UserId")] Jar jar)
        {
            if (ModelState.IsValid)
            {
                DbContext.Entry(jar).State = EntityState.Modified;
                DbContext.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(jar);
        }

        [HttpGet]
        public virtual ActionResult Delete(int id)
        {
            Jar jar = DbContext.Jars.Find(id);
            DbContext.Jars.Remove(jar);
            DbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
