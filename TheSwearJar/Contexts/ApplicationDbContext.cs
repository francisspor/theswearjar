﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using TheSwearJar.Models;

namespace TheSwearJar.Contexts
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("DefaultConnection") { }

        public DbSet<Jar> Jars { get; set; }
        public DbSet<Swear> Swears { get; set; }
        public DbSet<SwearInstance> SwearInstances { get; set; }
    }
}