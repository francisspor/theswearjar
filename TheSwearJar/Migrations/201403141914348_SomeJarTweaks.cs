namespace TheSwearJar.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SomeJarTweaks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Jars", "RecentlyActive", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Jars", "RecentlyActive");
        }
    }
}
