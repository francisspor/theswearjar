namespace TheSwearJar.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingUserId : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Swears", name: "User_Id", newName: "UserId");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Swears", name: "UserId", newName: "User_Id");
        }
    }
}
