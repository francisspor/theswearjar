﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TheSwearJar.Startup))]
namespace TheSwearJar
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
